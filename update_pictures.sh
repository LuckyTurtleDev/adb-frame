#!/bin/bash

adb shell rm -rf /mnt/sdcard/Pictures/wallpaper
adb push wallpaper /mnt/sdcard/Pictures/wallpaper
adb shell am broadcast -a android.intent.action.MEDIA_MOUNTED -d file:///sdcard
